
hostname=$1
fb=$2
linkedin=$3

now=`date`
mkdir "$hostname at $now"
cd "$hostname at $now"
mkdir about_me
cd about_me
mkdir personal
mkdir professional
cd personal
echo "https://www.facebook.com/$fb" > facebook.txt
cd .. 
cd professional
echo "https://www.linkedin.com/in/$linkedin" > linkedin.txt
cd ../..
mkdir my_friends
mkdir my_system_info

cd my_friends
curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt >> list_of_my_friends.txt
cd ..

cd my_system_info
echo 'My username : `uname`'> about_this_laptop.txt
echo 'With host: `uname -a`'>> about_this_laptop.txt

echo '`ping -c 3 google.com`' >internet_connection.txt

